import mongoose from 'mongoose'
const { ObjectId } = mongoose.Types


const OrganizationsSchema = new mongoose.Schema({
  addressReference: {
    type: String,
  },
    address: {
    type: String,
  },
  brand: {
    type: String,
  },
  type: {
    type: String,
    required: [true],
  },
  name: {
    type: String,
    required: [true],
  },
  features: {
    type: Array,
  },
  phones: {
      type: Array,
  },
  hours: {
    type: Array,
  },
  whatsapp: {
      type: Array,
  },
  facebook: {
      type: String,
  },
  instagram: {
      type: String,
  },
  email: {
      type: String,
  },
  city: {
    type: String,
  },
  nearby: {
    type: [{ type: ObjectId }],
  },
  web: {
    type: String,
  }
})
mongoose.models = {}

export default mongoose.models.Organizations || mongoose.model('Organizations', OrganizationsSchema, 'organizations')