import mongoose from 'mongoose'

const RegionsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true],
    },
    cities: {
        type: Array,
    }
})

export default mongoose.models.Regions || mongoose.model('Regions', RegionsSchema, 'regions')