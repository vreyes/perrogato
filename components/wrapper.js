import Head from 'next/head'
import Link from 'next/link'
import { config } from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css'

import styles from '../styles/wrapper.module.css'

config.autoAddCss = false

export default function Wrapper(props) {
    return(
        <div className={styles.container}>
            <Head>
                <script async src="https://www.googletagmanager.com/gtag/js?id=G-7E3DDY5LS9"></script>
                {process.env.NODE_ENV === 'production'?
                    <script
                        dangerouslySetInnerHTML={{
                        __html: `
                            window.dataLayer = window.dataLayer || [];
                            function gtag(){dataLayer.push(arguments);}
                            gtag('js', new Date());

                            gtag('config', 'G-7E3DDY5LS9', {
                                page_path: window.location.pathname
                            });
                            `
                        }}
                    />
                    :
                    ''
                }
                {process.env.NODE_ENV === 'production'?
                    <script
                        dangerouslySetInnerHTML={{
                        __html: `
                        (function(h,o,t,j,a,r){
                            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                            h._hjSettings={hjid:2476806,hjsv:6};
                            a=o.getElementsByTagName('head')[0];
                            r=o.createElement('script');r.async=1;
                            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                            a.appendChild(r);
                        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
                            `
                        }}
                    />
                    :
                    ''
                }
                <link 
                    rel="preconnect" 
                    href="https://fonts.gstatic.com"
                />
                <link 
                    media="print" onLoad="this.onload=null;this.removeAttribute('media');"
                    href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" 
                    rel="stylesheet"
                />
                <link 
                    href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha3/dist/css/bootstrap.min.css" 
                    rel="stylesheet" 
                    integrity="sha384-CuOF+2SnTUfTwSZjCXf01h7uYhfOBuxIhGKPbfEJ3+FqH/s6cIFN9bGr1HmAg4fQ" 
                    crossOrigin="anonymous"
                />
                <meta name="google-site-verification" content="eAXNRIE_aOd0hifhMj7AKF1bsccWtTsISDEnRdhcVcA" />
            </Head>
            <header className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
                <div className="container">
                    <Link href="/"><a><h1 className="h5 my-0 mr-md-auto fw-normal">🐶PerroGato😺</h1></a></Link>
                    <nav className="my-2 my-md-0 mr-md-3">
                        {/*<a className="p-2 text-dark" href="#">Features</a>*/}
                    </nav>
                </div>
            </header>
            
            <div className="container">
                {props.children}
            </div>
            <footer className="footer mt-auto py-3 bg-light">
              <div className="container">
                <div className={styles.footerContainer}>
                  <div>
                    <span className="text-muted">Escribenos en </span>
                    <Link href="/contacto">
                      <a>contacto</a>
                    </Link>
                  </div>
                  <div>
                    <a href="https://infopet.netlify.app/">nuevo hogar</a>
                  </div>
                </div>
              </div>
            </footer>
        </div>
    )

}