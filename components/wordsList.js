import styles from '../styles/wordsList.module.css'


const WordList = props => {
    return props.words.map((word, i) => 
        <span key={i} className={`${styles.word} badge bg-secondary`}>{word}</span>
    )
}

export default WordList