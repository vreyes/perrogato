const Emoji = props => {
    const {category} = props

    if(category === "empresa") return <span>🏢</span>
    if(category === "profesional") return <span>👩‍⚕️</span>
    if(category === "place") return <span>🏞️</span>
    if(category === "hospital") return <span>🏥</span>
    if(category === "belleza") return <span>💄</span>
    if(category === "hotel") return <span>🏨</span>
    if(category === "comercio") return <span>🛍</span>
    if(category === "accesorios") return <span>🦴</span>
    if(category === "educacion") return <span>📖</span>
    
    return ''
}

export default Emoji