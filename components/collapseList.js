import React, { useState } from 'react'
import styles from '../styles/collapseList.module.css'


const Item = props => {
    const [opened, setOpened] = useState(false)
    const {name, content} = props

    return (
        <li className={`list-group-item ${styles.list_group_item}`}>
            <a onClick={e => {e.preventDefault(); setOpened(!opened)}} className={styles.name}>{name}</a>
            <div style={{display: opened? 'block':'none'}}>
                {content}
            </div>
        </li>
    )
}


const CollapseList = props => {
    const {items} = props

    return (
        <ul className="list-group">
            {items?.map((item , i) => {
                return <Item 
                    key={i}
                    name={item.name}
                    content={item.content}
                />
            })}
        </ul>
    )
}

export default CollapseList