import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt, faPhoneAlt, faClock, faEnvelope } from '@fortawesome/free-solid-svg-icons'
import { faFacebook, faInstagram, faWhatsapp } from '@fortawesome/free-brands-svg-icons'
import Link from 'next/link'
import {prepareStringToLink} from '../utilities'
import styles from '../styles/customCard.module.css'

const PhonesList = ({ phones }) => {
    if (phones?.length && phones[0])
        return <li>
            <FontAwesomeIcon icon={faPhoneAlt} listItem className={styles.cardIcon} />
            {phones.map((p, i) => (<p key={i}><a href={`tel:${p}`} className="text-primary">{p}</a></p>))}
        </li>
    return ''
}

const HoursList = ({ hours }) => {
    if(hours?.length && hours[0])
        return <li>
            <FontAwesomeIcon icon={faClock} listItem className={styles.cardIcon} />
            {hours.map((h, i) => (<p key={i}>{h}</p>))}
        </li>
    return ''
}

const SocialIcons = ({ facebook, instagram }) => {
    return (
        <div className={styles.socialIcons}>
            {facebook?
                <a href={facebook} rel="nofollow">
                    <span className="fa-layers fa-fw">
                            <FontAwesomeIcon icon={faFacebook} />
                    </span>
                </a>
                :''
            }
            {instagram?
                <span className="fa-layers fa-fw">
                    <a href={instagram} rel="nofollow">
                        <FontAwesomeIcon icon={faInstagram} />
                    </a>
                </span>
                :''
            }
        </div>
    )
}


const CustomCard = props => {
    const {
        address,
        addressReference,
        brand,
        city,
        phones,
        whatsapp,
        email,
        facebook,
        hours,
        instagram,
        link, 
        name,
        places
    } = props.element
    const description = props.description || 'empresa' 
    let title='', children='', subtitle=''
    let cardWidth='col-md-4' 
    let border

    if(description === "empresa") {
        title = name
        children = <CompanyCard 
            address={address}
            addressReference={addressReference}
            city={city}
            phones={phones}
            whatsapp={whatsapp}
            hours={hours}
            email={email}
            facebook={facebook}
            instagram={instagram}
        />
    }
    if(description === "profesional") {
        title = name
        subtitle = brand
        children = <ProfessionalCard 
            address={address}
            addressReference={addressReference}
            phones={phones}
            hours={hours}
            email={email}
            facebook={facebook}
            instagram={instagram}
        />
    }
    if(description === "place") {
        children = <PlaceCard places={places} />
        cardWidth = 'col-md-12'
        border = styles.withoutBorder
    }
    const cardTitle = <h3 className={`card-title ${styles.cardTitle}`}>{title}</h3>

  return (
    <div className={`col-sm-12 ${cardWidth} ${styles.cardWrapper}`}>
      <div className={`card h-100 ${styles.card} ${border ? border : ""}`}>
        <div className="card-body">
          {link ? (
            <Link href={link}>
              <a target="_blank">{cardTitle}</a>
            </Link>
          ) : (
            cardTitle
          )}

          {subtitle ? (
            <h6 className="card-subtitle mb-2 text-muted">{subtitle}</h6>
          ) : (
            ""
          )}
          {children}
        </div>
      </div>
    </div>
  );
};

const CompanyCard = ({
  address,
  addressReference,
  city,
  phones,
  whatsapp,
  hours,
  email,
  facebook,
  instagram,
  web,
}) => {
  return (
    <div>
      <ul className={`fa-ul ${styles.ul}`}>
        {address ? (
          <li>
            <FontAwesomeIcon
              icon={faMapMarkerAlt}
              listItem
              className={styles.cardIcon}
            />
            <p style={{ marginBottom: 0 }}>{address}</p>
            {addressReference ? (
              <p className="text-muted">({addressReference})</p>
            ) : (
              ""
            )}
          </li>
        ) : (
          ""
        )}
        {phones ? <PhonesList phones={phones} /> : ""}
        {whatsapp && whatsapp.length ? (
          <li>
            <FontAwesomeIcon
              icon={faWhatsapp}
              listItem
              className={styles.cardIcon}
            />
            {/*<p style={{marginBottom: 0}}>{address}</p>*/}
            <a
              href={`https://api.whatsapp.com/send?phone=${whatsapp}`}
              className="text-primary"
            >
              {whatsapp}
            </a>
          </li>
        ) : (
          ""
        )}
        {hours && hours.length && hours[0] ? <HoursList hours={hours} /> : ""}
        {email ? (
          <li>
            <FontAwesomeIcon
              icon={faEnvelope}
              listItem
              className={styles.cardIcon}
            />
            <a href={`mailto:${email}`}>{email}</a>
          </li>
        ) : (
          ""
        )}
        {web ? (
          <li>
            <FontAwesomeIcon
              icon={faLink}
              listItem
              className={styles.cardIcon}
            />
            <a href={web} rel="nofollow">
              {web}
            </a>
          </li>
        ) : (
          ""
        )}
      </ul>
      <SocialIcons facebook={facebook} instagram={instagram} />
    </div>
  );
};

const ProfessionalCard = ({
    address,
    addressReference,
    phones,
    hours,
    email,
    facebook,
    instagram
}) => {

    return (
        <div>
            <ul className={`fa-ul ${styles.ul}`}>
                {address?
                    <li>
                        <FontAwesomeIcon icon={faMapMarkerAlt} listItem className={styles.cardIcon}/>
                        <p style={{marginBottom: 0}}>{address}</p>
                        {addressReference? <p className="text-muted">({addressReference})</p>:''}
                    </li>
                    :''
                }
                {phones?.length?
                        <PhonesList phones={phones} />
                        :''
                    }
                {hours?.length?
                    <HoursList hours={hours}/>
                    : ''
                }
                {email?
                    <li>
                        <FontAwesomeIcon icon={faEnvelope} listItem className={styles.cardIcon}/>
                        {email}
                    </li>
                    : ''
                }
            </ul>
            <SocialIcons 
                facebook={facebook}
                instagram={instagram}
            />
        </div>
    )
        
}



export {CustomCard, CompanyCard}