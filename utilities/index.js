const prepareStringToLink = (string) => {
  return string.replace(/ /g, "-").replace(/'/g, "");
};

const unprepareStringToLink = (string) => {
  return string.replace(/-/g, " ");
};

const makeLink = (strings) => {
  let link = "https://infopet.netlify.app/";

  strings.forEach((str) => {
    link += `/${prepareStringToLink(str)}`;
  });
  return link;
};

const makeVeterinaryLink = ({ name, address, city }) => {
  return makeLink(["ciudad", city, "clínicas-veterinarias", name, address]);
};

export {
  makeLink,
  makeVeterinaryLink,
  prepareStringToLink,
  unprepareStringToLink,
};
