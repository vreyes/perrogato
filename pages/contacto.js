import Wrapper from '../components/wrapper'
import Head from 'next/head'


export default function Contacto() {
    return (
        <Wrapper>
            <Head>
                <title>🐶😺 Los mejores datos para tus animales en tu ciudad</title>
                <meta name="description" content="Clínicas veterinarias, comida para perro, comida para gato y exóticos, alimento a domicilio, veterinarios a domicilio y todo en tu ciudad" />
            </Head>
            <div className="row">
                <div className="col">
                    <iframe 
                        src="https://docs.google.com/forms/d/e/1FAIpQLSc6AUnYeP8G_UKERvuxwCCyy70FuTYQI8cN0MMQIf_AVP92oA/viewform?embedded=true"
                        scrolling="no" 
                        width="100%"
                        height="630"
                        frameborder="0"
                        marginheight="0"
                        marginwidth="0"
                    >
                        Cargando…
                    </iframe>
               </div>
            </div>
        </Wrapper>
    )
}