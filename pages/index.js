import Wrapper from '../components/wrapper'
import CollapseList from '../components/collapseList'
import Head from 'next/head'
import Link from 'next/link'
import {prepareStringToLink} from '../utilities'
import dbConnect from '../utils/dbConnect'
import Regions from '../models/Regions'


import styles from '../styles/home.module.css'

export default function Home({ regions }) {
    const regionList = regions
        .filter(region => region.cities.length > 0 && region.cities[0])
        .map(region => {
            const links = region.cities.map((city, i) => <li key={i} style={{paddingTop: ".4rem"}}><Link href={`/ciudad/${prepareStringToLink(city)}`}>{city}</Link></li>)
            return {name: region.name, content: <ul>{links}</ul>}})

    return (
            <Wrapper>
                <Head>
                    <title>🐶😺 Los mejores datos para tus animales en tu ciudad</title>
                    <meta name="description" content="Clínicas veterinarias, comida para perro, comida para gato y exóticos, alimento a domicilio, veterinarios a domicilio y todo en tu ciudad" />
                </Head>
                <div className={`row ${styles.container}`}>
                    <div className={`col align-self-center ${styles.mainMenu}`}>
                        <h2>Encuentra lo mejor para tu mascota en tu ciudad</h2>
                        <h3>Selecciona tu ciudad</h3>
                        <CollapseList 
                            items={regionList}
                        />
                    </div>
                </div>
            </Wrapper>
    )
}

export async function getServerSideProps({ params }) {
    await dbConnect()
    const regions = await Regions.find({}, {_id: 0}).lean()

    return {
        props: {
            regions
        }
    }
}