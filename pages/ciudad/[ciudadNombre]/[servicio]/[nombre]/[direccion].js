import Head from 'next/head'
import Link from 'next/link'
import Wrapper from '../../../../../components/wrapper'
import Emoji from '../../../../../components/emoji'
import { CompanyCard } from '../../../../../components/customCard'
import WordsList from '../../../../../components/wordsList'
import Organizations from '../../../../../models/Organizations'
import { makeVeterinaryLink, unprepareStringToLink } from '../../../../../utilities'
import styles from '../../../../../styles/direccion.module.css'
import dbConnect from '../../../../../utils/dbConnect'
import mongoose from 'mongoose'

const { ObjectId } = mongoose.Types




function Feature(props) {
  const { feature } = props

  if (typeof feature === "object")
    return (
      <section className={`${styles.sectionContainer} col-md-4`}>
        <h3>
          <Emoji category={feature.type} />
          Servicio{feature.services ? "s" : ''} de {feature.name}
        </h3>
        <ul>
          {feature.services?.map((service, i) => (
            <li key={i}>{service}</li>
          ))}
        </ul>
      </section>
    )
  return (<h3>{feature}</h3>)
}


function Selling(props) {
  const { sell } = props

  if (typeof sell === "object")
    return (
      <section className={`${styles.sectionContainer} col-md-4`}>
        <h3><Emoji category={sell.type} />{sell.name}</h3>
        {sell.brands ?
          <WordsList words={sell.brands} />
          :
          ""
        }
      </section>
    )
  return (<h3>{sell}</h3>)
}


function Nearby({ clinics }) {
  if (clinics.length === 0) return ''
  return (
    <section className={`${styles.sectionContainer} col-md-4`}>
      <h3>Clínicas Cercanas</h3>
      {clinics?.map((clinic, i) => (
        <Link key={i} href={makeVeterinaryLink(clinic)}>
          <a>
            <h4 className={styles.near}>{clinic.name}</h4>
          </a>
        </Link>
      ))}
    </section>
  )
}


export default function Name({ veterinaryClinic }) {
  if (!veterinaryClinic) return ''
  veterinaryClinic.address = `${veterinaryClinic.address}`
  const {
    address,
    addressReference,
    city,
    email,
    facebook,
    features,
    hours,
    instagram,
    name,
    nearby,
    nearbyClinics,
    phones,
    selling,
    whatsapp,
  } = veterinaryClinic

  return (
    <Wrapper>
      <Head>
        <title>
          🐶😺 {name} en {city}
        </title>
        <meta
          name="description"
          content={`${name} en ${address}, ${city}`}
        />
      </Head>
      <div className="row">
        <div className={`col-12 ${styles.sectionContainer}`}>
          <h2>{name}</h2>
          <CompanyCard
            address={address}
            addressReference={addressReference}
            city={city}
            phones={phones}
            whatsapp={whatsapp}
            hours={hours}
            email={email}
            facebook={facebook}
            instagram={instagram}
          />
        </div>
        <div className="col-12">
          <div className="row">
            {features?.map((feature, i) =>
              <Feature key={i} feature={feature} />
            )}
            {selling?.map((sell, i) =>
              <Selling key={i} sell={sell} />
            )}
            {nearby && nearby.length > 0 && <Nearby clinics={nearbyClinics} />}
          </div>
        </div>
      </div>
    </Wrapper>
  )
}

export async function getServerSideProps({ params }) {
  await dbConnect()
  const city = unprepareStringToLink(params.ciudadNombre)
  const name = unprepareStringToLink(params.nombre)
  const address = unprepareStringToLink(params.direccion)
  const veterinaryClinic = JSON.parse(
    JSON.stringify(
      await Organizations.findOne({
        city, name, address, type: 'clinic'
      })
    )
  )
  const nearby = JSON.parse(
    JSON.stringify(
      await Organizations.find({
        "_id": veterinaryClinic.nearby.map(n => ObjectId(n))
      })
    )
  )
  veterinaryClinic.nearbyClinics = nearby
  return {
    props: {
      veterinaryClinic,
      city
    }
  }
}
