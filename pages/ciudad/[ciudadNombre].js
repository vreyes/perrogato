import Head from 'next/head'
import {useEffect, useState} from 'react'
import Wrapper from '../../components/wrapper'
import {CustomCard} from '../../components/customCard'
import Emoji from '../../components/emoji'
import { makeVeterinaryLink, unprepareStringToLink }  from '../../utilities'
import styles from '../../styles/ciudadNombre.module.css'
import dbConnect from '../../utils/dbConnect'
import Organizations from '../../models/Organizations'



export default function Ciudad ({
  city,
  veterinaryClinics,
  veterinaryPharmacies,
  veterinarians
}) {
  const [description, setDescription] = useState('')

  useEffect(() => {
    if(city) {
      const sections = [
        "clínicas veterinarias",
        "veterinarios a domicilio",
        "alimentos a domicilio",
        "farmacia veterinaria",
      ]

      setDescription(`🐶😺${sections.map((section, i) => {
        if(i === sections.length-2) return `${section} y `
        if(i === sections.length-1) return section
        return `${section}, `
      }).join('')} en ${city}`)
    }
  }, [city])

  if(!city) return ''

  return (
    <Wrapper>
      <Head>
        <title>🐶😺Todo para tu mascota en {city}</title>
        <meta name="description" content={description} />
      </Head>
      <div className="row">
        <div className="col-12">
          {veterinaryClinics?.length?
            <section className={styles.section}>
              <h2>
                <Emoji category={'empresa'} />
                Clínicas Veterinarias en {city}
              </h2>
              <div className="row">
                {veterinaryClinics.map((veterinaryClinic, i) => {
                  veterinaryClinic.link = makeVeterinaryLink(veterinaryClinic)
                  return (
                    <CustomCard
                      key={i}
                      element={veterinaryClinic}
                      city={city}
                    />
                  )
                })}
              </div>
            </section>
            :''
          }
          {veterinarians?.length ?
            <section className={styles.section}>
              <h2>
                <Emoji category={'profesional'} />
                Veterinarios a domicilio en {city}
              </h2>
              <div className="row">
                {veterinarians.map((veterinary, i) => {
                  return (
                    <CustomCard 
                      key={i}
                      element={veterinary}
                      city={city}
                      description={"profesional"}
                    />
                  )
                })}
              </div>
            </section>
            :''
          }
          {veterinaryPharmacies?.length ?
            <section className={styles.section}>
              <h2>
                <Emoji category={'empresa'} />
                Farmacias Veterinarias en {city}
              </h2>
              <div className="row">
                {veterinaryPharmacies.map((veterinaryPharmacy, i) => {
                  return (
                    <CustomCard
                      key={i}
                      element={veterinaryPharmacy}
                      city={city}
                    />
                  )
                })}
              </div>
            </section>
            :''
          }
        </div>
      </div>
    </Wrapper>
  )
}

export async function getServerSideProps({ params }) {
    await dbConnect()
    const city = unprepareStringToLink(params.ciudadNombre)
    const orgs = await Organizations.find(
      {city, available: {"$ne": false}}, {_id: 0, nearby: 0}
    ).lean()

    const clinics = orgs.filter(org => org.type === "clinic")
    const pharmacies = orgs.filter(org => org.type === "pharmacy")
    const doctors = orgs.filter(org => org.type === "doctor")

    return {
        props: {
            veterinaryClinics: clinics,
            veterinaryPharmacies: pharmacies,
            veterinarians: doctors,
            city 
        }
    }
}