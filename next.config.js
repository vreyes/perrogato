module.exports = {
  async redirects() {
    return [
      {
        source: "/",
        destination: "https://infopet.netlify.app",
        permanent: true,
        basePath: false,
      },
      {
        source: "/ciudad/:ciudadNombre",
        destination: "https://infopet.netlify.app/ciudad/:ciudadNombre",
        permanent: true,
        basePath: false,
      },
      {
        source: "/ciudad/:ciudadNombre/:servicio/:nombre/:direccion",
        destination:
          "https://infopet.netlify.app/ciudad/:ciudadNombre/:servicio/:nombre/:direccion",
        permanent: true,
        basePath: false,
      },
    ];
  },
};
